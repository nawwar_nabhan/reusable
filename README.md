# this repo used for reusable components

## SAM : Search Menu

it's a a menu (select) element contains search box for filter item

take two param (itemList , ChosenItem) pass the list off item with (itemList)
AND return the value of the clicked item in the menu with the (ChosenItem)

create with css and pure react Hook functionality
** for customize OR add style here is the main class to use **

1. containerWrapper : the whole component wrapper
2. MainContainer : the body that contain both dropDownList and the btn
3. ListShowed : the name that shown for chosen item
4. DropDownList : the section the contain the list of item and searchBox
5. SearchBox : input search container
6. ItemsContainer : list of items in the dropDown
7. SingleItem : each single item row

**_ end of SAM _**
