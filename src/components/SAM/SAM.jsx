import { useEffect, useRef, useState } from "react"
import "./sam.css"
const SAM = (
    {
        itemList ,
        ChosenItem
    }
) => {

    const [items , setItems] = useState(itemList)
    const [Open , SetOpen] = useState(false)
    const [Accepted , SetAccepted] = useState("")

    const BoxRef = useRef(null)

    const handleChange = (e)=>{
        let val = e.target.value.toLowerCase() ;
        const data = itemList.filter((el)=>{return (el.toLowerCase().includes(val))})
        setItems(data)
    }
    const Chose_Item_handler = (el)=>{
        SetAccepted(el);
        ChosenItem(el)
        SetOpen(false)
        setItems(itemList)
    }

    useEffect(()=>{
        const handler = (e)=>{
             if(!BoxRef?.current?.contains(e.target)){
                    SetOpen(false)
                    setItems(itemList)
                }
        }
        window.addEventListener("click" , handler)
    },[])
    return (
        <>
            <div className="containerWrapper" ref={BoxRef}>
                <div className="MainContainer">
                    <div className="ListShowed"
                        onClick={()=>{SetOpen(!Open)}}
                    >
                        {Accepted.length > 1 ? Accepted  : "chose"}
                    </div>

                    {Open  ? 
                    
                    <div className="DropDownList">
                        <div className="SearchBox">
                            <input type="text" onChange={handleChange}
                            placeholder="search for item"
                            />
                        </div>
                        <div className="ItemsContainer">
                            {items.map((el , rowId)=>{
                                return (
                                    <div className="SingleItem" key={rowId}
                                        onClick={()=>Chose_Item_handler(el)}>
                                        {el}
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                : null}
                </div>
            </div>
        </>
    )
}

export default SAM
